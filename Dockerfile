# Build with:
# docker build --tag=corona-api .
# Run with:
# docker run corona-api /bin/sh -c 'make test'
# Or for interactive shell:
# docker run -it --rm corona-api
FROM ubuntu:18.04

ENV USER="user"
ENV HOME_DIR="/home/${USER}"
ENV WORK_DIR="${HOME_DIR}/app"
ENV PORT=8000
EXPOSE $PORT

# configure locale
RUN apt update -qq > /dev/null \
    && apt install --yes --no-install-recommends \
    locales \
    && locale-gen en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*
ENV LANG="en_US.UTF-8" \
    LANGUAGE="en_US.UTF-8" \
    LC_ALL="en_US.UTF-8"

# install minimal system dependencies
RUN apt update -qq > /dev/null \
    && apt install --yes --no-install-recommends \
    make \
    sudo \
    && rm -rf /var/lib/apt/lists/*

# prepare non root env, with sudo access and no password
RUN useradd --create-home --home-dir ${HOME_DIR} --shell /bin/bash ${USER} \
    && usermod -append --groups sudo ${USER} \
    && echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
    && mkdir ${WORK_DIR} \
    && chown ${USER}:${USER} -R ${WORK_DIR}

USER ${USER}
WORKDIR ${WORK_DIR}

# install system dependencies
COPY Makefile requirements.txt ${WORK_DIR}/
RUN sudo apt update -qq > /dev/null \
    && sudo make system_dependencies \
    && sudo chown ${USER}:${USER} -R ${WORK_DIR} \
    && make virtualenv \ 
    && sudo rm ${WORK_DIR}/requirements.txt ${WORK_DIR}/Makefile \
    && sudo rm -rf /var/lib/apt/lists/*

COPY . ${WORK_DIR}

CMD venv/bin/uvicorn main:app --host 0.0.0.0 --port $PORT
