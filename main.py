from datetime import date, datetime, timedelta

from fastapi import FastAPI
from starlette.responses import RedirectResponse

from corona.corona import get_stats

app = FastAPI()


@app.get("/")
def root():
    return RedirectResponse(url='/docs')


@app.get("/stats/{country}")
def stats_country(country: str, date: date = None):
    yesterday = (datetime.now() - timedelta(days=1)).date()
    date = date or yesterday
    stats = get_stats(country, date)
    return stats
