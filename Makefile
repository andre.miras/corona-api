VIRTUAL_ENV ?= venv
PIP=$(VIRTUAL_ENV)/bin/pip
TOX=`which tox`
PYTHON=$(VIRTUAL_ENV)/bin/python
UVICORN=$(VIRTUAL_ENV)/bin/uvicorn
ISORT=$(VIRTUAL_ENV)/bin/isort
FLAKE8=$(VIRTUAL_ENV)/bin/flake8
BLACK=$(VIRTUAL_ENV)/bin/black
PYTEST=$(VIRTUAL_ENV)/bin/pytest
MYPY=$(VIRTUAL_ENV)/bin/mypy
SOURCES=mysodexo/ tests/ setup.py setup_meta.py
DOCS_DIR=docs
PYTHON_MAJOR_VERSION=3
PYTHON_MINOR_VERSION=8
SYSTEM_DEPENDENCIES= \
    build-essential \
    git \
    libpython$(PYTHON_VERSION)-dev \
    python$(PYTHON_VERSION) \
    virtualenv
PYTHON_VERSION=$(PYTHON_MAJOR_VERSION).$(PYTHON_MINOR_VERSION)
PYTHON_MAJOR_MINOR=$(PYTHON_MAJOR_VERSION)$(PYTHON_MINOR_VERSION)
PYTHON_WITH_VERSION=python$(PYTHON_VERSION)
DOCKER_TAG=corona-api


all: virtualenv

system_dependencies:
	apt install --yes --no-install-recommends $(SYSTEM_DEPENDENCIES)

$(VIRTUAL_ENV):
	virtualenv -p $(PYTHON_WITH_VERSION) $(VIRTUAL_ENV)
	$(PIP) install -r requirements.txt

virtualenv: $(VIRTUAL_ENV)

run: virtualenv
	$(UVICORN) main:app --reload

test: virtualenv
	# $(PYTEST) tests/

lint/isort-check: virtualenv
	$(ISORT) --check-only --recursive --diff $(SOURCES)

lint/isort-fix: virtualenv
	$(ISORT) --recursive $(SOURCES)

lint/black-fix: virtualenv
	$(BLACK) --verbose $(SOURCES)

lint/flake8: virtualenv
	$(FLAKE8) $(SOURCES)

lint/black-check: virtualenv
	$(BLACK) --check $(SOURCES)

lint/mypy: virtualenv
	$(MYPY) $(SOURCES)

lint: lint/isort-check lint/flake8 lint/black-check lint/mypy

clean:
	find . -type d -name "__pycache__" -exec rm -r {} +
	find . -type d -name "*.egg-info" -exec rm -r {} +

clean/all: clean
	rm -rf $(VIRTUAL_ENV)

docker/build:
	docker build -t $(DOCKER_TAG) -f Dockerfile .

docker/run:
	docker run --rm -p 8000:8000 $(DOCKER_TAG)

docker/shell:
	docker run --rm -it $(DOCKER_TAG) /bin/bash
